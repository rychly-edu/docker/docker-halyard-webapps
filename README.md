# Halyard WebApps Docker Image

[![pipeline status](https://gitlab.com/rychly-edu/docker/docker-halyard-webapps/badges/master/pipeline.svg)](https://gitlab.com/rychly-edu/docker/docker-halyard-webapps/commits/master)
[![coverage report](https://gitlab.com/rychly-edu/docker/docker-halyard-webapps/badges/master/coverage.svg)](https://gitlab.com/rychly-edu/docker/docker-halyard-webapps/commits/master)

The image is based on [rychly-docker/docker-hadoop-hbase-base](/rychly-edu/docker/docker-hadoop-hbase-base).
The version of the base image can be restricted on build by the `BASE_VERSION` build argument.

## Build

### The Latest Version by Docker

~~~sh
docker build --pull -t "registry.gitlab.com/rychly-edu/docker/docker-halyard-webapps:latest" .
~~~

### All Versions by the Build Script

~~~sh
./build.sh --build "registry.gitlab.com/rychly-edu/docker/docker-halyard-webapps" "latest"
~~~

For the list of versions to build see [docker-tags.txt file](docker-tags.txt).

## Configuration

Set environment variables. For example, see [docker-compose.yml file](docker-compose.yml).

## Run

~~~sh
docker-compose up
~~~
